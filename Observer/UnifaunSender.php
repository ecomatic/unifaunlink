<?php
/**
 * Created by PhpStorm.
 * User: Yuki
 * Date: 05/04/2018
 * Time: 09:44
 */

namespace Ecomatic\UnifaunLink\Observer;


use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Belvg\Seller\Helper\Seller;

class UnifaunSender implements ObserverInterface {

	protected $_customerRepository;
	protected $_scopeConfig;
	protected $_addressRepository;
	protected $_sellerHelper;

	public function __construct(
		CustomerRepositoryInterface $customerRepository,
		ScopeConfigInterface $scopeConfig,
		AddressRepositoryInterface $addressRepository,
		Seller $sellerHelper
	) {
		$this->_customerRepository = $customerRepository;
		$this->_scopeConfig        = $scopeConfig;
		$this->_addressRepository  = $addressRepository;
		$this->_sellerHelper       = $sellerHelper;
	}

	/**
	 * @param Observer $observer
	 *
	 * @return void
	 * @throws LocalizedException
	 */
	public function execute( Observer $observer ) {

		if ( ! $this->_getValue( 'enable' ) ) {
			// Module disabled
			return;
		}

		/** @var $order Order */
		$order = $observer->getEvent()->getOrder();
		if ( $order->getIsVirtual() ) {
			// No shipping for virtual orders
			return;
		}

		$sellerId = $order->getSellerId();


		if ( ! $sellerId ) {
			$str = date( 'Y-m-d H:i:s' ) . "\n";
			$str .= "Order no: " . $order->getIncrementId() . "\n";
			$str .= "no seller id\n\n";
			file_put_contents( 'var/log/unifaun.log', $str, 8 );

			return;
		}

		$sellerData = $this->_sellerHelper->getSellerData( $sellerId );

		if ( ! isset( $sellerData['type'] ) || $sellerData['type'] != 1 ) {
			//Seller type is not Grossist
			return;
		}

		$seller = $this->_customerRepository->getById( $sellerId );

		$sellerAddress = ( $seller->getDefaultBilling() != null ? $seller->getDefaultBilling() : $seller->getDefaultShipping() );
		if ( $sellerAddress == null ) {
			foreach ( $seller->getAddresses() as $adr ) {
				$sellerAddress = $adr;
				break;
			}
		} else {
			$sellerAddress = $this->_addressRepository->getById( $sellerAddress );
		}
		if ( $sellerAddress == null ) {
			$str = date( 'Y-m-d H:i:s' ) . "\n";
			$str .= "Order no: " . $order->getIncrementId() . "\n";
			$str .= "Seller has no addresses!\n\n";
			file_put_contents( 'var/log/unifaun.log', $str, 8);
			return;
		}
		$sellerStreet = $sellerAddress->getStreet();

		// Sender
//		$sender['quickId']  = $seller->getId();
		$sender['name']     = $seller->getFirstname() . ' ' . $seller->getLastname();
		$sender['address1'] = array_key_exists( 0, $sellerStreet ) ? $sellerStreet[0] : '';
		$sender['address2'] = array_key_exists( 1, $sellerStreet ) ? $sellerStreet[1] : '';
		$sender['zipcode']  = $sellerAddress->getPostcode();
		$sender['city']     = $sellerAddress->getCity();
		$sender['country']  = $sellerAddress->getCountryId();
		$sender['phone']    = $sellerAddress->getTelephone();
		$sender['email']    = $seller->getEmail();

		// Sender partners
		$sp = $this->_getValue( 'sender_partner_id' );
		$cn = $this->_getValue( 'customer_no' );
//		if ( isset( $sp ) && isset( $cn ) ) {
		$senderPartners = [
			[
				'id'     => $sp,
				'custNo' => strval($cn)
			]
		];

		$serviceId = $this->_getValue( 'service_id' );
		// Service
		$service['id'] = $serviceId;
//		}

		// Receiver
		$address              = $order->getShippingAddress();
		$receiver['name']     = $address->getName();
		$receiver['address1'] = array_key_exists( 0, $address->getStreet() ) ? $address->getStreet()[0] : '';
		$receiver['address2'] = array_key_exists( 1, $address->getStreet() ) ? $address->getStreet()[1] : '';
		$receiver['zipcode']  = '212 20'; //$address->getPostcode();
		$receiver['city']     = 'Malmö'; //$address->getCity();
		$receiver['country']  = $address->getCountryId();
		$receiver['phone']    = $address->getTelephone();
		$receiver['email']    = $address->getEmail();

		// Parcels
		$parcels = [
			[
				'copies'         => 1,
				'weight'         => ( $order->getWeight() != 0 ? $order->getWeight() : 3 ),
				'valuePerParcel' => true,
			]
		];

		// Creating the API payload
		$payload['shipment'] = [
			'sender'          => $sender,
			'senderPartners'  => $senderPartners,
			'service'         => $service,
			'receiver'        => $receiver,
			'parcels'         => $parcels,
			'orderNo'         => $order->getIncrementId(),
			'senderReference' => 'senderReference mandatory'
		];

		if ( $sd = $order->getSellerData() ) {
			if ( $date = json_decode( $sd )->date ) {
				if ( count( $a = explode( '/', $date ) ) == 3 ) {
					$payload['shipment']['deliveryDate'] = "$a[2]-$a[0]-$a[1]";
				}
			}
		}

		$payload['pdfConfig'] = [
			"target4XOffset" => 0,
			"target2YOffset" => 0,
			"target1Media"   => "laser-ste",
			"target1YOffset" => 0,
			"target3YOffset" => 0,
			"target2Media"   => "laser-a4",
			"target4YOffset" => 0,
			"target4Media"   => null,
			"target3XOffset" => 0,
			"target3Media"   => null,
			"target1XOffset" => 0,
			"target2XOffset" => 0
		];

		$this->_sendShipment( $payload );
	}

	protected function _sendShipment( $shipment ) {
		$ch = curl_init( $this->_getValue( 'url' ) );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $shipment ) );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'Authorization: Bearer ' . $this->_getValue( 'userid' ) . '-' . $this->_getValue( 'password' )
		] );
		curl_setopt( $ch, CURLOPT_HEADER, true );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

		$response = curl_exec( $ch );

		if( ! $response ) {
			file_put_contents('var/log/unifaun.log', 'Curl error: ' . curl_error($ch));
		}

		ob_start();
		var_dump( $response );
		file_put_contents( 'var/log/unifaun.log', ob_get_clean(), 8 );

		curl_close( $ch );
	}

	protected function _getValue( $key ) {
		return $this->_scopeConfig->getValue( 'ecomatic_unifaunlink/general/' . $key );
	}
}
